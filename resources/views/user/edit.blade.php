@extends('layouts.app')
@section('content')
@include('partials._message')
<div class="row">
    <div class="col-md-12">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Account Settings</div>
            <div class="panel-body">
                <form action="{{route('user.update', $user->id)}}" method="POST">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input required name="first_name" value="{{$user->first_name}}" class="form-control" placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input name="middle_name" value="{{$user->middle_name}}" class="form-control" placeholder="Middle Name">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input name="last_name" value="{{$user->last_name}}" class="form-control" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Address</label>
                                <textarea name="address" class="form-control">{{$user->address}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Mobile Number (+63 or 09)</label>
                                <input name="mobile_number" value="{{$user->mobile_number}}" class="form-control" placeholder="Mobile Number">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success form-control">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
                <h2>Transactions</h2>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Amount Paid</th>
                                <th>Date Transacted</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($trans as $x)
                            <tr>
                                <td>{{ $x->id }}</td>
                                <td>{{ $x->stock()->first()->name }}</td>
                                <td>{{ $x->amount_received }}</td>
                                <td>{{ $x->date_transaction }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
