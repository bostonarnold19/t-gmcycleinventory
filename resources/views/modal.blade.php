<div id="myModal-{{$key}}" class="modal fade" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="nameModal">{{ $key }} Service History</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Model No.</th>
                                <th>Engine No.</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>Inquiry Service</th>
                                <th>Amount</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($services as $service)
                            <tr>
                                <td>{{ $service->model_no }}</td>
                                <td>{{ $service->engine_no }}</td>
                                <td>{{ $service->address }}</td>
                                <td>{{ $service->email }}</td>
                                <td>{{ $service->contact_number }}</td>
                                <td>{{ $service->inquiry }}</td>
                                <td>{{ !empty($service->amount) ? $service->amount : 'Warranty' }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
