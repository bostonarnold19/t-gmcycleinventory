@extends('layouts.app')
@section('content')
@include('partials._message')
<div class="row">
    <a href="{{ route('stock.index')}}">
        <div class="col-md-4">
            <div class="well">
                <i class="fa fa-shopping-cart fa-4x fa-fw" aria-hidden="true"></i>
                <div class="text">
                    <h3>Stocks</h3>
                    <p>Items : {{ $stock_count }}</p>
                </div>
            </div>
        </div>
    </a>
    <a href="{{url('billing')}}">
        <div class="col-md-4">
            <div class="well">
                <i class="fa fa-shopping-basket fa-4x fa-fw" aria-hidden="true"></i>
                <div class="text">
                    <h3>Orders</h3>
                    <p>Total transaction : {{ $order_count }}</p>
                </div>
            </div>
        </div>
    </a>
    <a href="{{url('transactions')}}">
        <div class="col-md-4">
            <div class="well">
                <i class="fa fa-exchange fa-4x fa-fw" aria-hidden="true"></i>
                <div class="text">
                    <h3>Transaction</h3>
                    <p>Total transaction : {{ $transaction_count }}</p>
                </div>
            </div>
        </div>
    </a>
    <a href="{{route('service.index')}}">
        <div class="col-md-4">
            <div class="well">
                <i class="fa fa-cog fa-spin fa-4x fa-fw" aria-hidden="true"></i>
                <div class="text">
                    <h3>Service</h3>
                    <p>Total service : {{ $service_count }}</p>
                </div>
            </div>
        </div>
    </a>
    <a href="{{route('service.index')}}" onclick="document.getElementById('flush').submit();return false;">
        <div class="col-md-4">
            <div class="well">
                <i class="fa fa-trash fa-4x fa-fw" aria-hidden="true"></i>
                <div class="text">
                    <h3>Flush Wishlist</h3>
                    <p>Total pending orders : {{ $wishlist }}</p>
                </div>
            </div>
        </div>
    </a>
        <a href="{{route('at')}}">
        <div class="col-md-4">
            <div class="well">
                <i class="fa fa-user fa-4x fa-fw" aria-hidden="true"></i>
                <div class="text">
                    <h3>Audit Trails</h3>
                    <p>#</p>
                </div>
            </div>
        </div>
    </a>
    <a href="{{route('yo')}}">
        <div class="col-md-4">
            <div class="well">
                <i class="fa fa-cog fa-4x fa-fw" aria-hidden="true"></i>
                <div class="text">
                    <h3>Service History</h3>
                    <p>#</p>
                </div>
            </div>
        </div>
    </a>


    <form id="flush" action="{{route('flush.orders')}}" method="POST">
        {{csrf_field()}}
    </form>
</div>
@endsection
@section('style')
<style type="text/css">
.text {
    display:inline-block;
}
</style>
@endsection
