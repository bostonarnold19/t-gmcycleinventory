@extends('layouts.app')
@section('content')
<div class="row">
    <h2>Audit Trail</h2>
<br>
<br>
        <div class="panel panel-default">
<div class="table-responsive">
    <table class="table table-bordered" id="stock-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Log Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach($trails as $trail)
            <tr>
                <td>{{ $trail->title }}</td>
                <td>{{date('d M Y g:i:s A', strtotime($trail->created_at))}}</td>



            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
  <br>
    <br>
    <br>  <br>
    <br>
    <br>
@endsection
