@extends('layouts.app')
@section('content')
@include('partials._message')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Register</div>
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <label for="first_name" class="col-md-4 control-label">First Name</label>
                        <div class="col-md-6">
                            <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
                            @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                        <label for="middle_name" class="col-md-4 control-label">Middle Name</label>
                        <div class="col-md-6">
                            <input id="middle_name" type="text" class="form-control" name="middle_name" value="{{ old('middle_name') }}" required>
                            @if ($errors->has('middle_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('middle_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <label for="last_name" class="col-md-4 control-label">Last Name</label>
                        <div class="col-md-6">
                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                            @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

{{--                     <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                        <label for="contact" class="col-md-4 control-label">Contact</label>
                        <div class="col-md-6">
                            <input id="contact" type="text" class="form-control" name="contact" value="{{ old('contact') }}" required autofocus>
                            @if ($errors->has('contact'))
                            <span class="help-block">
                                <strong>{{ $errors->first('contact') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div> --}}
                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="col-md-4 control-label">Address</label>
                        <div class="col-md-6">
                            <textarea id="address"  class="form-control" name="address" required>{{ old('address') }}</textarea>
                            @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                        <label for="mobile_number" class="col-md-4 control-label">Mobile Number (+63 or 09)</label>
                        <div class="col-md-6">
                            <input id="mobile_number" type="text" class="form-control" name="mobile_number" value="{{ old('mobile_number') }}" required>
                            @if ($errors->has('mobile_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile_number') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                        <label for="birthday" class="col-md-4 control-label">Birthday</label>
                        <div class="col-md-6">
                            <input id="birthday" type="date" class="form-control" name="birthday" value="{{ old('birthday') }}" required>
                            @if ($errors->has('birthday'))
                            <span class="help-block">
                                <strong>{{ $errors->first('birthday') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label for="gender" class="col-md-4 control-label">Gender</label>
                                <div class="col-md-6">
                                <select class="form-control" name="gender" id="gender" required>
                                    <option disabled>Select Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                                </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Civil Status</label>
                                <div class="col-md-6">
                                <select class="form-control" name="status" id="status" required>
                                    <option disabled>Select Civil Status</option>
                                    <option value="single">Single</option>
                                    <option value="married">Married</option>
                                    <option value="divorced">Divorced</option>
                                    <option value="widowed">Widowed</option>
                                </select>
                                </div>
                    </div>
                    <div class="form-group{{ $errors->has('monthly_income') ? ' has-error' : '' }}">
                        <label for="monthly_income" class="col-md-4 control-label">Monthly Income</label>
                        <div class="col-md-6">
                            <input id="monthly_income" type="text" class="form-control" name="monthly_income" value="{{ old('monthly_income') }}" required>
                            @if ($errors->has('monthly_income'))
                            <span class="help-block">
                                <strong>{{ $errors->first('monthly_income') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('other_income_source') ? ' has-error' : '' }}">
                        <label for="other_income_source" class="col-md-4 control-label">Other Income Source</label>
                        <div class="col-md-6">
                            <input id="other_income_source" type="text" class="form-control" name="other_income_source" value="{{ old('other_income_source') }}" required>
                            @if ($errors->has('other_income_source'))
                            <span class="help-block">
                                <strong>{{ $errors->first('other_income_source') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('other_income_amount') ? ' has-error' : '' }}">
                        <label for="other_income_amount" class="col-md-4 control-label">Other Income Amount</label>
                        <div class="col-md-6">
                            <input id="other_income_amount" type="text" class="form-control" name="other_income_amount" value="{{ old('other_income_amount') }}" required>
                            @if ($errors->has('other_income_amount'))
                            <span class="help-block">
                                <strong>{{ $errors->first('other_income_amount') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('spouse_name') ? ' has-error' : '' }}">
                        <label for="spouse_name" class="col-md-4 control-label">Spouse Name</label>
                        <div class="col-md-6">
                            <input id="spouse_name" type="text" class="form-control" name="spouse_name" value="{{ old('spouse_name') }}" required>
                            @if ($errors->has('spouse_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('spouse_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('spouse_contact') ? ' has-error' : '' }}">
                        <label for="spouse_contact" class="col-md-4 control-label">Spouse Contact</label>
                        <div class="col-md-6">
                            <input id="spouse_contact" type="text" class="form-control" name="spouse_contact" value="{{ old('spouse_contact') }}" required>
                            @if ($errors->has('spouse_contact'))
                            <span class="help-block">
                                <strong>{{ $errors->first('spouse_contact') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('employer') ? ' has-error' : '' }}">
                        <label for="employer" class="col-md-4 control-label">Employer</label>
                        <div class="col-md-6">
                            <input id="employer" type="text" class="form-control" name="employer" value="{{ old('employer') }}" required>
                            @if ($errors->has('employer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('employer') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('company_address') ? ' has-error' : '' }}">
                        <label for="company_address" class="col-md-4 control-label">Company Address</label>
                        <div class="col-md-6">
                            <input id="company_address" type="text" class="form-control" name="company_address" value="{{ old('company_address') }}" required>
                            @if ($errors->has('company_address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('company_address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                        <label for="position" class="col-md-4 control-label">Position</label>
                        <div class="col-md-6">
                            <input id="position" type="text" class="form-control" name="position" value="{{ old('position') }}" required>
                            @if ($errors->has('position'))
                            <span class="help-block">
                                <strong>{{ $errors->first('position') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('employement_status') ? ' has-error' : '' }}">
                                <label for="employement_status" class="col-md-4 control-label">Employment Status</label>
                                <div class="col-md-6">
                                <select class="form-control" name="employement_status" id="employement_status" required>
                                    <option disabled>Select Status</option>
                                    <option value="male">Contractual</option>
                                    <option value="female">Regular</option>
                                </select>
                                </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                            Register
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
