@extends('layouts.app')
@section('content')
@include('partials._message')
<h2>Update service record</h2>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Update service record</h3>
            </div>
            <div class="panel-body">
                <form action="{{route('service.update', $service->id)}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input required name="name" value="{{$service->name}}" class="form-control" placeholder="name">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea placeholder="Address" required name="address" class="form-control">{{$service->address}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input name="email" type="email" value="{{$service->email}}" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Mobile Number (+63 or 09)</label>
                            <input required name="contact_number" value="{{$service->contact_number}}" class="form-control" placeholder="Contact Number">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Inquiry Services</label>
                            <select required name="inquiry" class="form-control">
                                <option {{ old('inquiry') === null ? 'selected' : '' }} disabled>Select inquiry</option>
                                <option value="Check Up" {{ old('inquiry') == 'Minor Tune Up' ? 'selected' : '' }}>Minor Tune Up</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Major Tune Up' ? 'selected' : '' }}>Major Tune Up</option>
                                <option value="Change Oil" {{ old('inquiry') == 'Top Overhaul' ? 'selected' : '' }}>Top Overhaul</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Major Engine Overhaul' ? 'selected' : '' }}>Major Engine Overhaul</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace/Lubricate Cable' ? 'selected' : '' }}>Replace/Lubricate Cable</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Brake Pads' ? 'selected' : '' }}>Replace Brake Pads</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Brake Shoe' ? 'selected' : '' }}>Replace Brake Shoe</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Front Oil Seal' ? 'selected' : '' }}>Replace Front Oil Seal</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Rear Hub Bearing' ? 'selected' : '' }}>Replace Rear Hub Bearing</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Battery Recharging' ? 'selected' : '' }}>Battery Recharging</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Head Light Bulb' ? 'selected' : '' }}>Replace Head Light Bulb</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Tail Light Bulb' ? 'selected' : '' }}>Replace Tail Light Bulb</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Panel Bulb' ? 'selected' : '' }}>Replace Panel Bulb</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Wiring Harness' ? 'selected' : '' }}>Replace Wiring Harness</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Drive Chain Sprocket' ? 'selected' : '' }}>Replace Drive Chain Sprocket</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Overhaul Carburetor' ? 'selected' : '' }}>Overhaul Carburetor</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Starter Motor' ? 'selected' : '' }}>Replace Starter Motor</option>
                                <option value="Tune Up" {{ old('inquiry') == 'Replace Cam Sprocket & Chain Set' ? 'selected' : '' }}>Replace Cam Sprocket & Chain Set</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Model No</label>
                            <input required name="model_no" value="{{$service->model_no}}" class="form-control" placeholder="Model Number">
                        </div>
                        <div class="form-group">
                            <label>Engine No</label>
                            <input required name="engine_no" value="{{$service->engine_no}}" class="form-control" placeholder="Engine Number">
                        </div>
                            <div class="form-group">
                            <label>Amount</label>
                            <input required id="amount" name="amount" value="{{$service->amount}}" class="form-control" placeholder="Amount">
                        </div>
                        <div class="form-group">
                            <input class="field" id="warranty" name="warranty" type="checkbox" value="Warranty">
                            <label>Warranty</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success form-control">Save</button>
                            <br>
                            <br>
                            <a href="{{route('service.show', $service->id)}}" class="btn btn-primary form-control">Print</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$('#warranty').on('change',function(){
if($('#warranty').prop('checked')) {
    $('#amount').prop( "disabled", true).val('');
}else {
    $('#amount').prop( "disabled", false);
}
});
</script>
@endsection
