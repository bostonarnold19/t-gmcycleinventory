@extends('layouts.app')
@section('content')
@include('partials._message')
<h2>Service record</h2>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i>Service Record</h3>
            </div>
            <div class="panel-body">
                <form action="{{route('service.update', $service->id)}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input required name="name" value="{{$service->name}}" class="form-control" placeholder="name">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea placeholder="Address" required name="address" class="form-control">{{$service->address}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input name="email" type="email" value="{{$service->email}}" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Mobile Number (+63 or 09)</label>
                            <input required name="contact_number" value="{{$service->contact_number}}" class="form-control" placeholder="Contact Number">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Inquiry Services</label>
                            <select required name="inquiry" class="form-control">
                                <option {{ $service->inquiry === null ? 'selected' : '' }} disabled>Select inquiry</option>
                                <option value="Check Up" {{ $service->inquiry == 'Check Up' ? 'selected' : '' }}>Check Up</option>
                                <option value="Tune Up" {{ $service->inquiry == 'Tune Up' ? 'selected' : '' }}>Tune Up</option>
                                <option value="Change Oil" {{ $service->inquiry == 'Change Oil' ? 'selected' : '' }}>Change Oil</option>
                                <option value="Spare Parts Replacement" {{ $service->inquiry == 'Spare Parts Replacement' ? 'selected' : '' }}>Spare Parts Replacement</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Model No</label>
                            <input required name="model_no" value="{{$service->model_no}}" class="form-control" placeholder="Model Number">
                        </div>
                        <div class="form-group">
                            <label>Engine No</label>
                            <input required name="engine_no" value="{{$service->engine_no}}" class="form-control" placeholder="Engine Number">
                        </div>
                            <div class="form-group">
                            <label>Amount</label>
                            <input required id="amount" name="amount" value="{{ empty($service->amount) ? 'Warranty' : $service->amount}}" class="form-control" placeholder="Amount">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    window.print();
</script>
@endsection
