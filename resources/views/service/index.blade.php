@extends('layouts.app')
@section('content')
@include('partials._message')
<div class="row">
    <div class="col-md-4">
        <div class="well">
            <h4>Service Reports</h4>
            <form action="{{ route('print.service') }}" method="get" target="_blank">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label" for="from">From:</label>
                            <input type="date" name="from" class="form-control" value="{{ date('Y-m-d') }}">
                        </div>
                        <div class="col-md-6">
                            <label class="control-label" for="to">To:</label>
                            <input type="date" name="to" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <br>
                            <button class="form-control btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<h2>Service Records</h2>
<a href="{{ route('service.create') }}" class="btn btn-success">Add Service Record</a>
<br>
<br>
<div class="table-responsive">
    <table class="table table-bordered" id="datatable">
        <thead>
            <tr>
                <th>Name</th>
                <th>Model No</th>
                <th>Engine No</th>
                <th>Inquiry</th>
                <th>Address</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($services as $service)
            <tr>
                <td>{{ $service->name }}</td>
                <td>{{ $service->model_no }}</td>
                <td>{{ $service->engine_no }}</td>
                <td>{{ $service->inquiry }}</td>
                <td>{{ $service->address }}</td>
                <td>{{ $service->email }}</td>
                <td>{{ $service->contact_number }}</td>
                <td>
                    <a href="{{route('service.edit', $service->id)}}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                    <button type="submit" form="service-delete-{{$service->id}}" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                    <form onsubmit="return confirm('Do you want to delete this data?');" id="service-delete-{{$service->id}}" action="{{route('service.destroy', $service->id)}}" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection
