@extends('layouts.app')
@section('content')
@include('partials._message')
<h2>Add service record</h2>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Add service record</h3>
            </div>
            <div class="panel-body">
                <form action="{{route('service.store')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input required name="name" value="{{old('name')}}" class="form-control" placeholder="name">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea placeholder="Address" required name="address" class="form-control">{{old('address')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input name="email" type="email" value="{{old('email')}}" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Mobile Number (+63 or 09)</label>
                            <input required name="contact_number" value="{{old('contact_number')}}" class="form-control" placeholder="Contact Number">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Inquiry Services</label>
                            <select required name="inquiry" class="form-control">
                                <option {{ old('inquiry') === null ? 'selected' : '' }} disabled>Select inquiry</option>
                                <option value="Minor Tune Up" {{ old('inquiry') == 'Minor Tune Up' ? 'selected' : '' }}>Minor Tune Up</option>
                                <option value="Major Tune Up" {{ old('inquiry') == 'Major Tune Up' ? 'selected' : '' }}>Major Tune Up</option>
                                <option value="Top Overhaul" {{ old('inquiry') == 'Top Overhaul' ? 'selected' : '' }}>Top Overhaul</option>
                                <option value="Major Engine Overhaul" {{ old('inquiry') == 'Major Engine Overhaul' ? 'selected' : '' }}>Major Engine Overhaul</option>
                                <option value="Replace/Lubricate Cable" {{ old('inquiry') == 'Replace/Lubricate Cable' ? 'selected' : '' }}>Replace/Lubricate Cable</option>
                                <option value="Replace Brake Pads" {{ old('inquiry') == 'Replace Brake Pads' ? 'selected' : '' }}>Replace Brake Pads</option>
                                <option value="Replace Brake Shoe" {{ old('inquiry') == 'Replace Brake Shoe' ? 'selected' : '' }}>Replace Brake Shoe</option>
                                <option value="Replace Front Oil Seal" {{ old('inquiry') == 'Replace Front Oil Seal' ? 'selected' : '' }}>Replace Front Oil Seal</option>
                                <option value="Replace Rear Hub Bearing" {{ old('inquiry') == 'Replace Rear Hub Bearing' ? 'selected' : '' }}>Replace Rear Hub Bearing</option>
                                <option value="Battery Recharging" {{ old('inquiry') == 'Battery Recharging' ? 'selected' : '' }}>Battery Recharging</option>
                                <option value="Replace Head Light Bulb" {{ old('inquiry') == 'Replace Head Light Bulb' ? 'selected' : '' }}>Replace Head Light Bulb</option>
                                <option value="Replace Tail Light Bulb" {{ old('inquiry') == 'Replace Tail Light Bulb' ? 'selected' : '' }}>Replace Tail Light Bulb</option>
                                <option value="Replace Panel Bulb" {{ old('inquiry') == 'Replace Panel Bulb' ? 'selected' : '' }}>Replace Panel Bulb</option>
                                <option value="Replace Wiring Harness" {{ old('inquiry') == 'Replace Wiring Harness' ? 'selected' : '' }}>Replace Wiring Harness</option>
                                <option value="Replace Drive Chain Sprocket" {{ old('inquiry') == 'Replace Drive Chain Sprocket' ? 'selected' : '' }}>Replace Drive Chain Sprocket</option>
                                <option value="Overhaul Carburetor" {{ old('inquiry') == 'Overhaul Carburetor' ? 'selected' : '' }}>Overhaul Carburetor</option>
                                <option value="Replace Starter Motor" {{ old('inquiry') == 'Replace Starter Motor' ? 'selected' : '' }}>Replace Starter Motor</option>
                                <option value="Replace Cam Sprocket & Chain Set" {{ old('inquiry') == 'Replace Cam Sprocket & Chain Set' ? 'selected' : '' }}>Replace Cam Sprocket & Chain Set</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Model No</label>
                            <input required name="model_no" value="{{old('model_no')}}" class="form-control" placeholder="Model Number">
                        </div>
                        <div class="form-group">
                            <label>Engine No</label>
                            <input required name="engine_no" value="{{old('engine_no')}}" class="form-control" placeholder="Engine Number">
                        </div>
                        <div class="form-group">
                            <label>Amount</label>
                            <input required id="amount" name="amount" value="{{old('amount')}}" class="form-control" placeholder="Amount">
                        </div>
                        <div class="form-group">
                            <input class="field" id="warranty" name="warranty" type="checkbox" value="Warranty">
                            <label>Warranty</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success form-control">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$('#warranty').on('change',function(){
if($('#warranty').prop('checked')) {
    $('#amount').prop( "disabled", true).val('');
}else {
    $('#amount').prop( "disabled", false);
}
});
</script>
@endsection
