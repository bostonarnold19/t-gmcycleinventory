@extends('layouts.app')
@section('content')
<h2>Transactions: From {{date("M jS, Y", strtotime($date['from']))}} - To {{date("M jS, Y", strtotime($date['to']))}}</h2>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                   <th>Name</th>
                   <th>Model No</th>
                <th>Engine CC</th>
                <th>Inquiry</th>
                <th>Address</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th>Amount Received</th>
            </tr>
        </thead>
        <tbody>
            @foreach($services as $service)
            <tr>
                      <td>{{ $service->id }}</td>
                      <td>{{ $service->name }}</td>
                <td>{{ $service->model_no }}</td>
                <td>{{ $service->engine_no }}</td>
                <td>{{ $service->inquiry }}</td>
                <td>{{ $service->address }}</td>
                <td>{{ $service->email }}</td>
                <td>{{ $service->contact_number }}</td>
                <td>{{ empty($service->amount) ? 'Waranty' : $service->amount }}</td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><b>TOTAL AMOUNT RECEIVED</b></td>
                <td><b>{{ $total_amount_recevied }}</b></td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
@section('script')
<script>
    window.print();
</script>
@endsection
