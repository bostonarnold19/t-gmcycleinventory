@extends('layouts.app')
@section('content')
@include('partials._message')
<h2>Services</h2>
<div class="row">
<br>
 <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i>Services Record</h3>
            </div>
<div class="table-responsive">
    <table class="table table-bordered" id="datatable">
        <thead>
            <tr>
                <th>Service Name</th>
                <th>AT</th>
                <th>Underbone</th>
                <th>Business/Sports</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Minor Tune Up</td>
                <td>150</td>
                <td>350</td>
                <td>150</td>
            </tr>
            <tr>
                <td>Major Tune Up</td>
                <td>250</td>
                <td>500</td>
                <td>250</td>
            </tr>
            <tr>
                <td>Top Overhaul</td>
                <td>450</td>
                <td>550</td>
                <td>450</td>
            </tr>
            <tr>
                <td>Major Engine Overhaul</td>
                <td>1250</td>
                <td>1500</td>
                <td>1500</td>
            </tr>
            <tr>
                <td>Replace/Lubricate Cable</td>
                <td>100</td>
                <td>150</td>
                <td>120</td>
            </tr>
            <tr>
                <td>Replace Brake Pads</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
            </tr>
            <tr>
                <td>Replace Brake Shoe</td>
                <td>120</td>
                <td>100</td>
                <td>100</td>
            </tr>
            <tr>
                <td>Replace Front Oil Seal</td>
                <td>200</td>
                <td>200</td>
                <td>250</td>
            </tr>
            <tr>
                <td>Replace Rear Hub Bearing</td>
                <td>150</td>
                <td>150</td>
                <td>100</td>
            </tr>
            <tr>
                <td>Battery Recharging</td>
                <td>30</td>
                <td>30</td>
                <td>50</td>
            </tr>
            <tr>
                <td>Replace Head Light Bulb</td>
                <td>100</td>
                <td>100</td>
                <td>100</td>
            </tr>
            <tr>
                <td>Replace Tail Light Bulb</td>
                <td>100</td>
                <td>100</td>
                <td>50</td>
            </tr>
            <tr>
                <td>Replace Panel Bulb</td>
                <td>150</td>
                <td>150</td>
                <td>100</td>
            </tr>
            <tr>
                <td>Replace Wiring Harness</td>
                <td>350</td>
                <td>350</td>
                <td>350</td>
            </tr>
            <tr>
                <td>Replace Drive Chain Sprocket</td>
                <td>200</td>
                <td>150</td>
                <td>200</td>
            </tr>
            <tr>
                <td>Overhaul Carburetor</td>
                <td>250</td>
                <td>300</td>
                <td>250</td>
            </tr>
            <tr>
                <td>Replace Starter Motor</td>
                <td>150</td>
                <td>150</td>
                <td>150</td>
            </tr>
            <tr>
                <td>Replace Cam Sprocket & Chain Set</td>
                <td>350</td>
                <td>450</td>
                <td>350</td>
            </tr>
        </tbody>
    </table>
</div><br>
<br>
</div>
</div>
</div>

@endsection
@section('script')
<script>
$('#warranty').on('change',function(){
if($('#warranty').prop('checked')) {
    $('#amount').prop( "disabled", true).val('');
}else {
    $('#amount').prop( "disabled", false);
}
});
</script>
@endsection
