<div class="modal fade" tabindex="-1" id="modal_edit-{{$item->id}}" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Reservation</h4>
      </div>
      <div class="modal-body">
        <form onsubmit="return confirm('Visit the store within 48hours to complete reservation');" method="POST" action="{{url('add/order/stock')}}">
          {{ csrf_field() }}
          <input type="hidden" value="{{$item->id}}" name="id">
          <div class="form-group">
            <label>Interest Rate</label>
            <select required name="quantity" class="form-control">
              <option {{ old('quantity') === null ? 'selected' : '' }} disabled>Choose</option>
              <option value="0" {{ old('quantity') == 0 ? 'selected' : '' }}>Full Payment</option>
              <option value="1" {{ old('quantity') == 1 ? 'selected' : '' }}>12 Months (1%)</option>
              <option value="2" {{ old('quantity') == 2 ? 'selected' : '' }}>24 Months (2%)</option>
              <option value="3" {{ old('quantity') == 3 ? 'selected' : '' }}>36 Months (3%)</option>
            </select>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success form-control">Reserve</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
