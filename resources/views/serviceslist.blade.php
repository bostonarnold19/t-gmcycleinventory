@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <h1>List Of Services</h1>
        <ul>
            <li>Minor Tune Up</li>
            <li>Major Tune Up</li>
            <li>Top Overhaul</li>
            <li>Major Engine Overhaul</li>
            <li>Replace/Lubricate Cable</li>
            <li>Replace Brake Pads</li>
            <li>Replace Brake Shoe</li>
            <li>Replace Front Oil Seal</li>
            <li>Replace Rear Hub Bearing</li>
            <li>Battery Recharging</li>
            <li>Replace Head Light Bulb</li>
            <li>Replace Tail Light Bulb</li>
            <li>Replace Panel Bulb</li>
            <li>Replace Wiring Harness</li>
            <li>Replace Drive Chain Sprocket</li>
            <li>Overhaul Carburetor</li>
            <li>Replace Starter Motor</li>
            <li>Replace Cam Sprocket & Chain Set</li>
        </ul>
    </div>
    <div class="col-md-3"></div>


</div>
  <br>
    <br>
    <br>
@endsection
