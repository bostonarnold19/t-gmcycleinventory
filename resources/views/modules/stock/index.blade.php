@extends('layouts.app')
@section('content')
@include('partials._message')
<h2>Stocks</h2>
<a href="{{ route('stock.create') }}" class="btn btn-success">New Stock</a>
<br>
<br>
<div class="table-responsive">
    <table class="table-bordered" id="stock-table">
        <thead>
            <tr>
                <th>Type</th>
                <th>Name</th>
                <th>Part No</th>
                <th>Model No</th>
                <th>Engine No</th>
                <th>Frame No</th>
                <th>Color</th>
                <th>Brand</th>
                {{-- <th>Initial Quantity</th> --}}
                <th>Price</th>
                {{-- <th>Initial Price</th> --}}
                {{-- <th>Discount</th> --}}
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($stocks as $stock)
            <tr>
                <td>{{ $stock->type }}</td>
                <td>{{ $stock->name }}</td>
                <td>{{ $stock->part_no }}</td>
                <td>{{ $stock->model_no }}</td>
                <td>{{ $stock->engine_no }}</td>
                <td>{{ $stock->frame_no }}</td>
                <td>{{ $stock->color }}</td>
                <td>{{ $stock->quantity }}</td>
                {{-- <td>{{ $stock->initial_quantity }}</td> --}}
                <td>{{ $stock->price }}</td>
                {{-- <td>{{ $stock->initial_price }}</td> --}}
                {{-- <td>{{ $stock->discount }}</td> --}}
                <td>
                    <a href="{{route('stock.edit', $stock->id)}}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>

                    @if(empty($stock->order()->where('status', '!=', 'wishlist')->first()))
                    <button type="submit" form="stock-delete-{{$stock->id}}" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                    @endif

                    <form onsubmit="return confirm('Do you want to delete this data?');" id="stock-delete-{{$stock->id}}" action="{{route('stock.destroy', $stock->id)}}" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#stock-table').DataTable();
    });
</script>
@endsection
