@extends('layouts.app')
@section('content')
@include('partials._message')
<div class="container">
    <div class="row">
        <h2>Create Stock</h2>
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="{{ route('stock.update', $stock->id) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="id" value="{{$stock->id}}">
                    <div class="row">
                        <div class="col s12">
                            <center>
                            <img src="{{ asset('images/'.$stock->image) }}" alt="avatar" id="avatar" height="300" width="300">
                            <input type="file" name="img_src" id="img_src" class="hide"  accept="image/x-png,image/gif,image/jpeg">
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="type">Type</label>
                                <select name="type" class="form-control" id="type">
                                    <option value="" disabled="">Select Type</option>
                                    <option value="motor" {{ $stock->type == "motor" ? "selected" : null }}>Motor</option>
                                    <option value="part"  {{ $stock->type == "part" ? "selected" : null }}>Part</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" value="{{$stock->name}}" name="name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="part_no">Part No</label>
                                <input disabled type="text" class="form-control" value="{{$stock->part_no}}" id="part_no" name="part_no">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="model_no">Model No</label>
                                <input disabled type="text" class="form-control" value="{{$stock->model_no}}" id="model_no" name="model_no">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="engine_no">Engine No</label>
                                <input disabled type="text" class="form-control" value="{{$stock->engine_no}}" id="engine_no" name="engine_no">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="frame_no">Frame No</label>
                                <input disabled type="text" class="form-control" value="{{$stock->frame_no}}" id="frame_no" name="frame_no">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="color">Color</label>
                                <input disabled type="text" class="form-control" value="{{$stock->color}}" id="color" name="color">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="type">Brand</label>
                                <select class="form-control" id="quantity"  name="quantity">
                                    {{-- <option value="" selected="" disabled="">Select Brand</option> --}}
                                    <option value="Honda" {{$stock->quantity == 'Honda' ? 'selected' : '' }}>Honda</option>
                                    <option value="Yamaha" {{$stock->quantity == 'Yamaha' ? 'selected' : '' }}>Yamaha</option>
                                    <option value="Kymco" {{$stock->quantity == 'Kymco' ? 'selected' : '' }}>Kymco</option>
                                    <option value="Kawasaki" {{$stock->quantity == 'Kawasaki' ? 'selected' : '' }}>Kawasaki</option>
                                    <option value="Suzuki" {{$stock->quantity == 'Suzuki' ? 'selected' : '' }}>Suzuki</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="initial_quantity">Downpayment</label>
                                <input type="text" class="form-control" value="{{$stock->initial_quantity}}" id="initial_quantity" name="initial_quantity">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" value="{{$stock->price}}" id="price" name="price">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="frame">Frame</label>
                                <input type="text" class="form-control" value="{{$stock->frame}}" required id="frame" name="frame">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="frame_type">Frame type</label>
                                <input type="text" class="form-control" value="{{$stock->frame_type}}" required id="frame_type" name="frame_type">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fuel_system">Fuel System</label>
                                <input type="text" class="form-control" value="{{$stock->fuel_system}}" required id="fuel_system" name="fuel_system">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dry_weight">Dry Weight(Without oil & fuel)</label>
                                <input type="text" class="form-control" value="{{$stock->dry_weight}}" required id="dry_weight" name="dry_weight">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dimensions">Overall Dimensions(Lengt x width x height)</label>
                                <input type="text" class="form-control" value="{{$stock->dimensions}}" required id="dimensions" name="dimensions">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="rear_suspension">Rear Suspension</label>
                                <input type="text" class="form-control" value="{{$stock->rear_suspension}}" required id="rear_suspension" name="rear_suspension">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="compression_ratio">Compression Ratio</label>
                                <input type="text" class="form-control" value="{{$stock->compression_ratio}}" required id="compression_ratio" name="compression_ratio">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="seat_height">Seat Height</label>
                                <input type="text" class="form-control" value="{{$stock->seat_height}}" required id="seat_height" name="seat_height">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="maximum_torque">Maximum Torque</label>
                                <input type="text" class="form-control" value="{{$stock->maximum_torque}}" required id="maximum_torque" name="maximum_torque">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="transmission_type">Transmission Type</label>
                                <input type="text" class="form-control" value="{{$stock->transmission_type}}" required id="transmission_type" name="transmission_type">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="front_tire">Front Tire</label>
                                <input type="text" class="form-control" value="{{$stock->front_tire}}" required id="front_tire" name="front_tire">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="front_suspension">Front Suspension</label>
                                <input type="text" class="form-control" value="{{$stock->front_suspension}}" required id="front_suspension" name="front_suspension">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fuel_capacity">Fuel Capacity(L)</label>
                                <input type="text" class="form-control" value="{{$stock->fuel_capacity}}" required id="fuel_capacity" name="fuel_capacity">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="displacement">Displacement(cc)</label>
                                <input type="text" class="form-control" value="{{$stock->displacement}}" required id="displacement" name="displacement">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <input type="text" class="form-control" value="{{$stock->category}}" required id="category" name="category">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="clutch_type">Clutch type</label>
                                <input type="text" class="form-control" value="{{$stock->clutch_type}}" required id="clutch_type" name="clutch_type">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="lubrication_system">Lubrication System</label>
                                <input type="text" class="form-control" value="{{$stock->lubrication_system}}" required id="lubrication_system" name="lubrication_system">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="minimum_ground_clearance">Minimum Ground Clearance</label>
                                <input type="text" class="form-control" value="{{$stock->minimum_ground_clearance}}" required id="minimum_ground_clearance" name="minimum_ground_clearance">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wheelbase">Wheelbase</label>
                                <input type="text" class="form-control" value="{{$stock->wheelbase}}" required id="wheelbase" name="wheelbase">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="rear_tire">Rear Tire</label>
                                <input type="text" class="form-control" value="{{$stock->rear_tire}}" required id="rear_tire" name="rear_tire">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="gear_shift_pattern">Gear Shift Pattern</label>
                                <input type="text" class="form-control" value="{{$stock->gear_shift_pattern}}" required id="gear_shift_pattern" name="gear_shift_pattern">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="maximum_horse_power">Maximum Horse Power</label>
                                <input type="text" class="form-control" value="{{$stock->maximum_horse_power}}" required id="maximum_horse_power" name="maximum_horse_power">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="brake_system">Brake System(Front/rear)</label>
                                <input type="text" class="form-control" value="{{$stock->brake_system}}" required id="brake_system" name="brake_system">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="starting_system">Starting System</label>
                                <input type="text" class="form-control" value="{{$stock->starting_system}}" required id="starting_system" name="starting_system">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="engine_oil">Engine Oil</label>
                                <input type="text" class="form-control" value="{{$stock->engine_oil}}" required id="engine_oil" name="engine_oil">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="iginition_type">Ignition Type</label>
                                <input type="text" class="form-control" value="{{$stock->iginition_type}}" required id="iginition_type" name="iginition_type">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="bore">Bore x Stroke(mm)</label>
                                <input type="text" class="form-control" value="{{$stock->bore}}" required id="bore" name="bore">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="engine_type">Engine Type</label>
                                <input type="text" class="form-control" value="{{$stock->engine_type}}" required id="engine_type" name="engine_type">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="reduction">Primary/Secondary Reduction Ratio</label>
                                <input type="text" class="form-control" value="{{$stock->reduction}}" required id="reduction" name="reduction">
                            </div>
                        </div>
                        {{--          <div class="col-md-4">
                            <div class="form-group">
                                <label for="initial_price">Initial Price</label>
                                <input type="text" class="form-control" value="{{$stock->initial_price}}" id="initial_price" name="initial_price">
                            </div>
                        </div> --}}
                        {{--          <div class="col-md-4">
                            <div class="form-group">
                                <label for="discount">Discount (%)</label>
                                <input type="text" class="form-control" value="{{$stock->discount}}" id="discount" name="discount">
                            </div>
                        </div> --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="remarks">Remarks</label>
                                <textarea style="resize: none;" class="form-control" rows="5" id="remarks" name="remarks">{{$stock->remarks}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-success form-control">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$('#type').on('change',function(){
var type = $('#type').val();
if (type === 'motor') {
$('#model_no').prop( "disabled", false);
$('#initial_quantity').prop( "disabled", false).val('');
$('#engine_no').prop( "disabled", false);
$('#frame_no').prop( "disabled", false);
$('#color').prop( "disabled", false);
$('#frame').prop( "disabled", false).val('');
$('#frame_type').prop( "disabled", false).val('');
$('#fuel_system').prop( "disabled", false).val('');
$('#dry_weight').prop( "disabled", false).val('');
$('#dimensions').prop( "disabled", false).val('');
$('#rear_suspension').prop( "disabled", false).val('');
$('#compression_ratio').prop( "disabled", false).val('');
$('#seat_height').prop( "disabled", false).val('');
$('#maximum_torque').prop( "disabled", false).val('');
$('#transmission_type').prop( "disabled", false).val('');
$('#front_tire').prop( "disabled", false).val('');
$('#front_suspension').prop( "disabled", false).val('');
$('#fuel_capacity').prop( "disabled", false).val('');
$('#displacement').prop( "disabled", false).val('');
$('#headlight').prop( "disabled", false).val('');
$('#category').prop( "disabled", false).val('');
$('#clutch_type').prop( "disabled", false).val('');
$('#lubrication_system').prop( "disabled", false).val('');
$('#fuel_system').prop( "disabled", false).val('');
$('#minimum_ground_clearance').prop( "disabled", false).val('');
$('#wheelbase').prop( "disabled", false).val('');
$('#rear_tire').prop( "disabled", false).val('');
$('#gear_shift_pattern').prop( "disabled", false).val('');
$('#maximum_horse_power').prop( "disabled", false).val('');
$('#brake_system').prop( "disabled", false).val('');
$('#starting_system').prop( "disabled", false).val('');
$('#engine_oil').prop( "disabled", false).val('');
$('#iginition_type').prop( "disabled", false).val('');
$('#bore').prop( "disabled", false).val('');
$('#engine_type').prop( "disabled", false).val('');
$('#reduction').prop( "disabled", false).val('');
$('#part_no').prop( "disabled", false).val('');
}else{
$('#model_no').prop( "disabled", true).val('');
$('#engine_no').prop( "disabled", true).val('');
$('#frame_no').prop( "disabled", true).val('');
$('#color').prop( "disabled", true).val('');
$('#initial_quantity').prop( "disabled", true).val('');
$('#part_no').prop( "disabled", false);
$('#frame').prop( "disabled", true).val('');
$('#frame_type').prop( "disabled", true).val('');
$('#fuel_system').prop( "disabled", true).val('');
$('#dry_weight').prop( "disabled", true).val('');
$('#dimensions').prop( "disabled", true).val('');
$('#rear_suspension').prop( "disabled", true).val('');
$('#compression_ratio').prop( "disabled", true).val('');
$('#seat_height').prop( "disabled", true).val('');
$('#maximum_torque').prop( "disabled", true).val('');
$('#transmission_type').prop( "disabled", true).val('');
$('#front_tire').prop( "disabled", true).val('');
$('#front_suspension').prop( "disabled", true).val('');
$('#fuel_capacity').prop( "disabled", true).val('');
$('#displacement').prop( "disabled", true).val('');
$('#headlight').prop( "disabled", true).val('');
$('#category').prop( "disabled", true).val('');
$('#clutch_type').prop( "disabled", true).val('');
$('#lubrication_system').prop( "disabled", true).val('');
$('#fuel_system').prop( "disabled", true).val('');
$('#minimum_ground_clearance').prop( "disabled", true).val('');
$('#wheelbase').prop( "disabled", true).val('');
$('#rear_tire').prop( "disabled", true).val('');
$('#gear_shift_pattern').prop( "disabled", true).val('');
$('#maximum_horse_power').prop( "disabled", true).val('');
$('#brake_system').prop( "disabled", true).val('');
$('#starting_system').prop( "disabled", true).val('');
$('#engine_oil').prop( "disabled", true).val('');
$('#iginition_type').prop( "disabled", true).val('');
$('#bore').prop( "disabled", true).val('');
$('#engine_type').prop( "disabled", true).val('');
$('#reduction').prop( "disabled", true).val('');
}
});
$('#avatar').on('click',function(){
$('#img_src').click();
});
function readURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function(e) {
$('#avatar').attr('src', e.target.result);
}
reader.readAsDataURL(input.files[0]);
}
}
$("#img_src").change(function() {
readURL(this);
});
</script>
@endsection
