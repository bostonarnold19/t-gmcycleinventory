@extends('layouts.app')
@section('content')
@include('partials._message')
<div class="container">
    <div class="row">
        <h2>Billing</h2>
        <div class="panel panel-default">
            <div class="panel-body">
                <input type="hidden" name="order_id" value="{{$order->id}}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="type">User</label>
                            <span class="form-control">
                                @if(!empty($order->user()->first()))
                                {{ $order->user()->first()->first_name }} {{ $order->user()->first()->middle_name }} {{ $order->user()->first()->last_name }}
                                @endif
                            </span>
                            <input type="hidden" name="user_id" value="{{$order->user()->first()->id}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <span class="form-control">{{ !empty($order->stock()->first()) ? $order->stock()->first()->name : ''}}</span>
                            <input type="hidden" name="stock_id" value="{{ !empty($order->stock()->first()) ? $order->stock()->first()->id : '' }}">
                        </div>
                    </div>
                         <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Type</label>
                            <span class="form-control">{{ !empty($order->stock()->first()) ? $order->stock()->first()->type : ''}}</span>
                            <input type="hidden" name="stock_id" value="{{ !empty($order->stock()->first()) ? $order->stock()->first()->id : '' }}">
                        </div>
                    </div>
                                      @php
                                $perc = '.'.$order->quantity;
                                $months = 0;

                                $dagdag= ((float) ($order->stock->price - $order->stock->initial_quantity) * (float) $perc);
                                if($order->quantity == 1) {
                                    $months = 12;
                                $x = (($order->stock->price + $dagdag) - $order->stock->initial_quantity) / $months;

                                } elseif($order->quantity == 2) {
                                    $months = 24;
                                $x = (($order->stock->price + $dagdag) - $order->stock->initial_quantity) / $months;

                                } elseif($order->quantity == 3) {
                                    $months = 36;
                                $x = (($order->stock->price + $dagdag) - $order->stock->initial_quantity) / $months;
                                } else {
                                    $x = $order->stock->price;
                                }
                            @endphp
                            @if($months != 0)
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="quantity">Monthly Due</label>
                            <span class="form-control">{{ number_format($x, 2) }}</span>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="quantity">Downpayment</label>
                            <span class="form-control">{{ number_format($order->stock->initial_quantity, 2) }}</span>
                        </div>
                    </div>

                    @if($order->transactions->count() == 0)
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="price">Price</label>
                            <span class="form-control">
                                @if(!empty($order->stock()->first()))
                                {{number_format($order->stock()->first()->price,2)}}
                                @endif
                            </span>
                        </div>
                    </div>
                    @else
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="price">Price</label>
                            <span class="form-control">
                                @if(!empty($order->stock()->first()))
                                {{number_format($order->stock()->first()->price - $order->stock->initial_quantity,2)}}
                                @endif
                            </span>
                        </div>
                    </div>

                    @endif

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="price">Interest</label>
                            <span class="form-control">
                                {{ ((float) ($order->stock->price - $order->stock->initial_quantity) * (float) $perc) }}
                            </span>
                        </div>
                    </div>
  @php
                             $asdasd= ((float) ($order->stock->price - $order->stock->initial_quantity) * (float) $perc);

                            @endphp
                    @if($order->transactions()->count() == 0)
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="price">Remaining Balance</label>
                            @if($order->status === "completed")
                            <span class="form-control" id="total_amount">0.00</span>
                            @else
                            <span class="form-control" id="total_amount">{{ $price + $asdasd  }}</span>
                            @endif
                        </div>
                    </div>
                    @else
      <div class="col-md-6">
                        <div class="form-group">
                            <label for="price">Remaining Balance</label>
                            @if($order->status === "completed")
                            <span class="form-control" id="total_amount">0.00</span>
                            @else

                            <span class="form-control" id="total_amount">{{ $price }}</span>
                            @endif
                        </div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
        @if(auth()->user()->user_type === 'Admin' && $order->status !== "completed")
            <div class="panel panel-default">
                <div class="panel-body">
                        @if($order->stock->type == 'motor')
                    <form action="" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="order_id" value="{{$order->id}}">
                        <input type="hidden" name="user_id" value="{{ !empty($order->user()->first()) ? $order->user()->first()->id : '' }}">
                        <input type="hidden" name="stock_id" value="{{ !empty($order->stock()->first()) ?  $order->stock()->first()->id : '' }}">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="amount_received">Amount Received</label>
                                <input required type="text" class="form-control" value="" oninput="validateNumber(this);" id="amount_received" name="amount_received">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="total">Remaining Balance</label>
                                <input type="text" class="form-control" readonly value="" id="total" name="total">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="date_transaction">Date Transaction</label>
                                <input type="date" class="form-control"  id="date_transaction" value="{{date('Y-m-d')}}" name="date_transaction">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-success form-control">Submit Transaction</button>
                            </div>
                        </div>
                    </form>
                        @endif

                    @if(!$order->transactions()->count())
                    <form action="{{ url('/billing/discount') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="order_id" value="{{$order->id}}">
                        <input type="hidden" name="user_id" value="{{ !empty($order->user()->first()) ? $order->user()->first()->id : '' }}">
                        <input type="hidden" name="stock_id" value="{{ !empty($order->stock()->first()) ? $order->stock()->first()->id : '' }}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-primary form-control">Pay in Full</button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        @endif
    </div>
    <div class="row">
        <a target="_blank" href="{{route('tadow', $order->id)}}" class="btn btn-primary form-control">Print</a>
        <Br>
        <Br>
      @if($order->status == 'on-going')
        <form onsubmit="return confirm('Do you want to add penalty to this transaction?')" action="{{route('penalty')}}" method="POST">
            {{csrf_field()}}
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <button type="submit" class="form-control btn btn-danger">Add Penalty</button>
        </form>
        @endif


        <hr>
        <div class="panel panel-default">
            <div class="panel-body">
                <table id="billing-table">
                    <thead>
                        <tr>
                            <th>Transaction ID</th>
                            <th>Amount Received</th>
                            <th>Remaining Balance</th>
                            <th>Date Transaction</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transactions as $t)
                        <tr>
                            <td>gmycle-0000-{{ $t->id }}</td>
                            <td>{{ $t->amount_received }}</td>
                            <td>{{ $t->total }}</td>
                            <td>{{ $t->date_transaction }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#billing-table').DataTable();
    });


    $('[id=amount_received]').on('keyup', function(){
        var amount = $(this).val();
        var tot = $('#total_amount').html();
        var amm = tot - amount;
        $('#total').val(amm.toFixed(2));
    });

    var validNumber = new RegExp(/^\d*\.?\d*$/);
    var lastValid = document.getElementById("amount_received").value;
    function validateNumber(elem) {
          if (validNumber.test(elem.value)) {
            lastValid = elem.value;
          } else {
            elem.value = lastValid;
          }
    }

</script>
@endsection
