@extends('layouts.app')
@section('content')
<h2>Orders</h2>
<div class="table-responsive">
    <table class="table-bordered" id="order-table">
        <thead>
            <tr>
                <th>User</th>
                <th>Contact Number</th>
                <th>Type</th>
                <th>Name</th>
                <th>Price</th>
                <th>Status</th>
                <th>Date Ordered</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>
                <td>
                    @if(!empty($order->user()->first()))
                    {{ $order->user()->first()->first_name }} {{ $order->user()->first()->middle_name }} {{ $order->user()->first()->last_name }}
                    @endif
                </td>
                 <td>
                    @if(!empty($order->user()->first()))
                    {{ $order->user()->first()->mobile_number }}
                    @endif
                </td>
                <td>
                    @if(!empty($order->stock()->first()))
                    {{ $order->stock()->first()->type }}
                    @endif
                </td>
                <td>
                    @if(!empty($order->stock()->first()))
                    {{ $order->stock()->first()->name }}
                    @endif
                </td>
                <td>
                    @if(!empty($order->stock()->first()))
                    {{ $order->stock()->first()->price }}
                    @endif
                </td>
             {{--    <td>
                    @if(!empty($order->stock()->first()))
                    {{ $order->stock()->first()->discount }}
                    @endif
                </td> --}}
                <td>{{ $order->status }}</td>
                <td>{{ date("M j, Y", strtotime($order->created_at)) }}</td>
                <td>
                    <a href="{{ url('billing/edit/'. $order->id) }}">
                    <button class="btn btn-success">
                        View transactions
                    </button>
                    </a>
                       @if($order->status == 'wishlist')
                    <form action="{{ route('order.destroy', $order->id)}}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                             <button class="btn btn-danger">
                                Revoke
                    </button>
                    </form>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#order-table').DataTable();
    });
</script>
@endsection
