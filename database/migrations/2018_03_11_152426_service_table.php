<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string("model_no")->nullable();
            $table->string("engine_no")->nullable();
            $table->string("inquiry")->nullable();
            $table->string("name")->nullable();
            $table->string("email")->nullable();
            $table->string("contact_number")->nullable();
            $table->longtext("address")->nullable();
            $table->string("amount")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');

    }
}
