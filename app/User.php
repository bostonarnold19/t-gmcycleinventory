<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'contact',
        'address',
        'email',
        'password',
        'mobile_number',
        'birthday',
        'gender',
        'status',
        'monthly_income',
        'other_income_source',
        'other_income_amount',
        'spouse_name',
        'spouse_contact',
        'employer',
        'company_address',
        'position',
        'employement_status',
        'user_type',
        'is_active',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function transactions()
    {
        return $this->hasMany('App\Transaction', 'user_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'user_id');
    }
}
