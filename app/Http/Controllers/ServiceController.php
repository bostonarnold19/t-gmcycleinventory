<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Session;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::all();
        return view('service.index', compact('services'));
    }

    public function create()
    {
        return view('service.create');
    }
    public function view()
    {
        return view('service.viewservices');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:191|email',
            'address' => 'required|max:191',
            'name' => 'required|max:191',
            'inquiry' => 'required|max:191',
            'model_no' => 'required|max:191',
            'engine_no' => 'required|max:191',
            'amount' => 'integer',
            'contact_number' => ['required', 'regex:/^(09|\+639)\d{9}$/'],
        ]);

        if (empty($request->amount)) {

            $service = Service::where('model_no', $request->model_no)->get();
            if ($service->count() > 5) {

                return redirect()->route('service.index')->withErrors('All warranty claimed');

            }

        }

        $request->request->add(['date_transaction' => date('Y-m-d')]);
        $service = new Service;
        $service->fill($request->all())->save();
        Session::flash('flash_message', 'service has been added!');
        return redirect()->route('service.index');
    }

    public function show($id)
    {
        $service = Service::find($id);
        return view('service.report', compact('service'));
    }

    public function edit($id)
    {
        $service = Service::find($id);
        return view('service.edit', compact('service'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required|max:191|email',
            'address' => 'required|max:191',
            'name' => 'required|max:191',
            'inquiry' => 'required|max:191',
            'model_no' => 'required|max:191',
            'engine_no' => 'required|max:191',
            'amount' => 'integer',
            'contact_number' => ['required', 'regex:/^(09|\+639)\d{9}$/'],
        ]);

        $service = Service::find($id);
        $service->update($request->all());
        Session::flash('flash_message', 'service has been updated!');
        return redirect()->route('service.index');
    }

    public function destroy($id)
    {
        $service = Service::find($id);
        $service->delete();
        Session::flash('flash_message', 'service has been deleted!');
        return redirect()->back();
    }
}
