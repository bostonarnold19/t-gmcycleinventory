<?php

namespace App\Http\Controllers;

use App\Order;
use App\Service;
use App\Stock;
use App\Transaction;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $stock;
    public function __construct(Stock $stock)
    {
        $this->middleware('auth');
        $this->stock = $stock;
        $this->service = Service::class;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function xx(Request $request)
    {
        $data = $request->all();
        $date = array(
            'from' => $data['from'],
            'to' => empty($data['to']) ? $data['from'] : $data['to'],
        );
        $services = Service::whereBetween('date_transaction', array($date['from'], $date['to']))->get();
        $total_amount_recevied = $services->sum('amount');

        return view('service.service', compact('services', 'total_amount_recevied', 'date'));
    }

    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {
        $stock_count = $this->stock->count();
        $order_count = Order::count();
        $transaction_count = Transaction::count();
        $service_count = Service::count();
        $wishlist = Order::where('status', 'wishlist')->count();
        return view('dashboard', compact('wishlist', 'service_count', 'stock_count', 'order_count', 'transaction_count'));
    }

    public function cart()
    {
        $xx = \Auth::user()->orders()->get()->toArray();
        $orders = [];
        foreach ($xx as $key => $order) {
            $stock = $this->stock->where('id', $order['stock_id'])->first()->toArray();
            $orders[$key] = $stock;
            $x = array(
                'status' => $order['status'],
                'order_id' => $order['id'],
            );
            $orders[$key] = array_merge($orders[$key], $x);
        }
        return view('modules.cart.index', compact('orders'));
    }
}
