<?php

namespace App\Http\Controllers;

use App\Order;
use App\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{

    public function flushorders()
    {
        $orders = Order::where('status', 'wishlist')->get();
        $counter = 0;
        foreach ($orders as $order) {
            if (Carbon::parse($order->created_at)->addHours(48)->lte(Carbon::now())) {
                $order->delete();
                $counter++;
            }
        }
        if ($counter) {
            Session::flash('flash_message', $counter . ' Orders has been flushed!');
        }
        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mv()
    {
        return view('mission_vision');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function welcome()
    {
        $dMotors = Stock::where('quantity', '!=', '0')->where('type', 'motor')->get();
        $motors = [];
        $check = ['completed', 'on-going'];
        foreach ($dMotors as $v) {
            if ($v->order()->first()) {
                $order = $v->order()->first();
                if (in_array($order->status, $check)) {
                    continue;
                }
            }
            $motors[] = $v;
        }
        $dParts = Stock::where('quantity', '!=', '0')->where('type', 'part')->get();
        $parts = [];
        foreach ($dParts as $v) {
            if ($v->order()->first()) {
                $order = $v->order()->first();
                if (in_array($order->status, $check)) {
                    continue;
                }
            }
            $parts[] = $v;
        }
        return view('welcome', compact('motors', 'parts'));
    }

    public function addOrder(Request $request)
    {
        if (empty($request->id)) {
            return response()->json(['error' => 'Bad Request'], 403);
        }
        $user = \Auth::user();
        $temp_discount = 0;
        if (empty($request->quantity)) {
            $temp_discount = 0;
        } else {
            $temp_discount = $request->quantity;
        }

        $data = [
            'user_id' => \Auth::id(),
            'stock_id' => $request->id,
            'quantity' => $temp_discount,
            'status' => 'wishlist',
        ];
        $exist = Order::where('user_id', $data['user_id'])->where('stock_id', $data['stock_id'])->count();
        if ($exist) {
            return response()->json(['error' => 'Bad Request'], 403);
        }
        $order = new Order;
        $order->fill($data)->save();
        return redirect()->back();
    }

    public function removeOrder(Request $request)
    {
        if (empty($request->id)) {
            return response()->json(['error' => 'Bad Request'], 403);
        }
        $user = \Auth::user();
        $data = [
            'user_id' => \Auth::id(),
            'stock_id' => $request->id,
        ];
        $exist = Order::where('user_id', $data['user_id'])->where('stock_id', $data['stock_id'])->first();
        $del = Order::find($exist->id);
        $del->delete();
        return response()->json();

    }
}
