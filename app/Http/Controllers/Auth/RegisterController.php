<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    // use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('auth.register');

    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'middle_name' => 'max:255',
            'contact' => 'max:255',
            'address' => 'required|max:100',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'mobile_number' => ['required', 'regex:/^(09|\+639)\d{9}$/'],
            'birthday' => 'required|max:255',
            'gender' => 'required|max:255',
            'status' => 'required|max:255',
            'monthly_income' => 'required|integer|',
            'other_income_source' => 'required|max:255',
            'other_income_amount' => 'required|integer',
            'spouse_name' => 'required|max:255',
            'spouse_contact' => 'required|max:255',
            'employer' => 'required|max:255',
            'company_address' => 'required|max:255',
            'position' => 'required|max:255',
            'employement_status' => 'required|max:255',
        ]);
        $user = new User;
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['is_active'] = 1;
        $user->fill($data)->save();
        return redirect()->route('login');
    }

    // /**
    //  * Get a validator for an incoming registration request.
    //  *
    //  * @param  array  $data
    //  * @return \Illuminate\Contracts\Validation\Validator
    //  */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [

    //     ]);
    // }

    // /**
    //  * Create a new user instance after a valid registration.
    //  *
    //  * @param  array  $data
    //  * @return \App\User
    //  */
    // protected function create(array $data)
    // {
    //     return User::create([
    //         'first_name' => $data['first_name'],
    //         'last_name' => $data['last_name'],
    //         'middle_name' => $data['middle_name'],
    //         'contact' => $data['contact'],
    //         'address' => $data['address'],
    //         'email' => $data['email'],
    //         'mobile_number' => $data['mobile_number'],
    //         'birthday' => $data['birthday'],
    //         'gender' => $data['gender'],
    //         'status' => $data['status'],
    //         'monthly_income' => $data['monthly_income'],
    //         'other_income_source' => $data['other_income_source'],
    //         'other_income_amount' => $data['other_income_amount'],
    //         'spouse_name' => $data['spouse_name'],
    //         'spouse_contact' => $data['spouse_contact'],
    //         'employer' => $data['employer'],
    //         'company_address' => $data['company_address'],
    //         'position' => $data['position'],
    //         'employement_status' => $data['employement_status'],
    //         'user_type' => 'Customer',
    //         'is_active' => true,
    //         'password' => bcrypt($data['password']),
    //     ]);
    // }
}
