<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Trail;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm(Request $request)
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            $new = new Trail;
            $data = [
                'title' => auth()->user()->first_name . ' ' . auth()->user()->last_name . ' ' . 'has logged in',
            ];
            $new->fill($data)->save();

            return redirect()->route('motor.index');
        } else {
            return back();
        }
    }

    public function logout()
    {

        $new = new Trail;
        $data = [
            'title' => auth()->user()->first_name . ' ' . auth()->user()->last_name . ' ' . 'has logged out',
        ];
        $new->fill($data)->save();
        Auth::logout();
        return redirect()->route('login');
    }
}
