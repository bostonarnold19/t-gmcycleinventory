<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Session;

class BrandController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $trans = $user->transactions;
        return view('user.edit', compact('user', 'trans'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'middle_name' => 'max:255',
            'mobile_number' => ['required', 'regex:/^(09|\+639)\d{9}$/'],
            'address' => 'required|max:100',

        ]);

        $user = User::find($id);
        $user->fill($request->all())->save();
        Session::flash('flash_message', 'User has been update!');
        return redirect()->back();

    }

}
