<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'name',
        'model_no',
        'engine_no',
        'frame_no',
        'part_no',
        'color',
        'image',
        'quantity',
        'initial_quantity',
        'remarks',
        'price',
        'initial_price',
        'discount',
        'type',
        'frame',
        'frame_type',
        'fuel_system',
        'dry_weight',
        'dimensions',
        'rear_suspension',
        'compression_ratio',
        'seat_height',
        'maximum_torque',
        'transmission_type',
        'front_tire',
        'front_suspension',
        'fuel_capacity',
        'displacement',
        'headlight',
        'category',
        'clutch_type',
        'lubrication_system',
        'fuel_system',
        'minimum_ground_clearance',
        'wheelbase',
        'rear_tire',
        'gear_shift_pattern',
        'maximum_horse_power',
        'brake_system',
        'starting_system',
        'engine_oil',
        'iginition_type',
        'bore',
        'engine_type',
        'reduction',
    ];

    public function order()
    {
        return $this->hasOne('App\Order', 'stock_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'product_id');
    }
}
