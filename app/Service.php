<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'model_no',
        'engine_no',
        'inquiry',
        'name',
        'email',
        'contact_number',
        'address',
        'date_transaction',
        'amount',
    ];
}
